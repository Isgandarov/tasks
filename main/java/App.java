package main;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String string;
        Scanner scan = new Scanner(System.in);
        string = scan.nextLine();
        string += " ";
        Implementation implementation = new Implementation(string);
        System.out.println(implementation.calculateSum());
    }
}
