package main;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    @Test
    public void test1() {
        Implementation implementation = new Implementation("1 2b 3");
        assertEquals(implementation.calculateSum(), 4);
    }

    @Test
    public void test2() {
        Implementation implementation = new Implementation("1b 2dhdfhfd 3b fhdfdhfd");
        assertEquals(implementation.calculateSum(), 0);
    }

    @Test
    public void test3() {
        Implementation implementation = new Implementation("-464 hdhdhdh 34b56");
        assertEquals(implementation.calculateSum(), 0);
    }

    @Test
    public void test4() {
        Implementation implementation = new Implementation("1 2 3");
        assertEquals(implementation.calculateSum(), 6);
    }

}