package main;

public class Implementation {
    private String string;

    public Implementation(String string) {
        this.string = string;
    }

    public int calculateSum() {
        String temp = "";
        int main_sum = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {

                temp += string.charAt(i);

            } else {
                main_sum += Check(temp);
                temp = "";
            }
        }
        return main_sum;
    }

    public int Check(String s) {
        try {
            if (Integer.parseInt(s) > 0)
                return Integer.parseInt(s);
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }


}